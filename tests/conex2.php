<?php

$dsn = 'mysql:dbname=mvc17;host=127.0.0.1';
$usuario = 'root';
$contraseña = 'root';

try {
    $mbd = new PDO($dsn, $usuario, $contraseña);
    $mbd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die ('Falló la conexión: ' . $e->getMessage());
}


echo "Conexión establecida!!";
echo "<hr>";
echo "<pre>";
var_dump($mbd);
echo "</pre>";

