<?php
define(DB_HOST, 'localhost');
define(DB_PASS, 'root');
define(DB_USER, 'root');
define(DB_DATABASE, 'mvc17');


//abrir conexión, se pasan unas constantes como parámetros:
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
//si da error salir del script con die() (equivale a exit())
if($mysqli->connect_errno > 0){
    die("Imposible conectarse con la base de datos [" . $mysqli->connect_error . "]");
}



echo "Conexión establecida!!";
echo "<hr>";
echo "<pre>";
var_dump($mysqli);
echo "</pre>";

