<?php

$dsn = 'mysql:dbname=mvc17;host=127.0.0.1';
$usuario = 'root';
$contraseña = 'root';

try {
    $db = new PDO($dsn, $usuario, $contraseña);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die ('Falló la conexión: ' . $e->getMessage());
}

echo "<h2>Conectar y hacer un SELECT</h2>";
echo "Conexión establecida<hr>";

echo "Uso de fetch";
$stmt = $db->query('SELECT * FROM users');
    echo "<ul>";
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    echo "<li>Fila $key: $row[name] $row[surname]</li>";
    }
    echo "</ul>";




$stmt = $db->query('SELECT * FROM users');
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo "Uso de fetchAll";
echo "<ul>";
foreach ($results as $key => $row) {
    echo "<li>Fila $key: $row[name] $row[surname]</li>";
}
echo "</ul>";

echo "<hr>";

echo "<h2>Tarea para el alumno</h2>";
echo "Busca en php.net y modifica este script:";
    echo "<ul>";
    echo "<li>Separa la vista en un fichero mixto HTML-PHP</li>";
    echo "<li>Busca en php.net y cuestra el número de columnas y filas sin hacer us de fetch o fetchAll</li>";
    echo "</ul>";




