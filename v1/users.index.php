<!DOCTYPE html>
<html>
<head>
    <title>Lista de usuarios</title>
</head>
<body>
    <header>Cabecera <hr></header>
    <content>
        <h1>Lista de usuarios</h1>
        <ul>
            <?php foreach ($users as $user): ?>
                <li><?php echo $user['name'] ?></li>
            <?php endforeach ?>
        </ul>
    </content>
    <footer> <hr> Pie</footer>
</body>
</html>
