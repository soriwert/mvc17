<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title>Lista de usuarios</title>
</head>
<body>
    <header>Cabecera <hr></header>
    <content>
        <h1>Lista de usuarios</h1>
        <ul>
            <?php foreach ($data as $user): ?>
                <li>
                    <?php
                    echo "$user->name $user->surname ";
                    echo "($user->email)";
                    ?>
                </li>
            <?php endforeach ?>
        </ul>
    </content>
    <footer> <hr> Pie</footer>
</body>
</html>
