<?php
require_once 'UserController.php';

$app = new UserController();

if(isset($_GET['method'])) {
    $method = $_GET['method'];
} else {
    $method = 'index';
}
if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = null;
}


if (method_exists($app, $method)) {
    $app->$method($id);
} else {
    die('Método no valido');
}


// http://localhost/mvc17/v1/index.php?method=delete
