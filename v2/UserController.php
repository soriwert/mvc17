<?php
/**
*
*/
require_once('User.php');

class UserController
{

    function __construct()
    {
        // echo "UserController";
    }

    public function render($view, $data)
    {
        $users = $data;
        require($view);
    }


    public function index()
    {
        // die('index');
        $users = User::all();
        // var_dump($users);
        $this->render('users.index.php', $users);
    }

    public function show($id)
    {
        $user = User::find($id);
        // var_dump($user);
        $this->render('users.show.php', $user);
    }

    public function delete($id)
    {
        die('borre el user ' . $id);
    }

    public function create()
    {
        die('formulario de alta');
    }

    public function store()
    {
        die('recoger formulario anterior');
    }

    public function edit($id)
    {
        die('formulario de edicion ' . $id);
    }

    public function update()
    {
        die('recoger formulario anterior');
    }
}
